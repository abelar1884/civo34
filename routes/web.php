<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*-----------------------ОСНОВНЫЕ СТРАНИЦЫ---------------------------*/
Route::get('/', 'MainController@index')->name('index');

Route::get('/competitions', 'MainController@competitions')->name('competitions');


//РАЗЛОГИНИВАНИЕ
Route::get('logout', function (){
    Auth::logout();
    return redirect('/login');
});

//АКТИВАЦИЯ АККАУНКТА
Route::get('/activation', 'ActivateController@activate')->name('activation');



/*-----------------------АДМИНКА---------------------------*/
Route::group(['middleware' => 'guest',], function () {

    Route::get('/admin', 'AdminController@index')->name('sb-index');

    //Роуты Конкурсы
    Route::resource('competition', 'CompetitionController');

    //Роуты Новости
    Route::resource('news', 'NewsController');

    //Роуты Мероприятия
    Route::resource('event', 'EventController');

    //Роуты Услуги
    Route::resource('service', 'ServiceController');

    //Роуты Сотрудники
    Route::resource('collaborator', 'CollaboratorController');

    //Роуты Вакансии
    Route::resource('vacancy', 'VacancyController');

    //Роуты Пользователи
    Route::resource('user', 'UserController');

    //Роуты Галерея
    Route::resource('gallery', 'GalleryController');

    //Роуты История Успеха
    Route::resource('story', 'StoryController');

    //Роуты Вопрос-Ответ
    Route::resource('faq', 'FAQController');

    //Роуты Партнеры
    Route::resource('partner', 'PartnerController');


});

/*-----------------------ЛИЧНЫЙ КАБИНЕТ---------------------------*/
Route::group(['middleware' => 'user',], function () {

    Route::get('/home', 'PersonalArea@index')->name('home');

    //Роуты Заявки
    Route::resource('bid', 'BidController');

});


/*-------------------------НЕПОНЯТНАЯ ШЛЯПА-------------------------*/

Auth::routes();
