<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('field1')->nullable($value =null);
            $table->string('field2')->nullable($value =null);
            $table->string('field3')->nullable($value =null);
            $table->string('field4')->nullable($value =null);
            $table->string('field5')->nullable($value =null);
            $table->string('field6')->nullable($value =null);
            $table->string('field7')->nullable($value =null);
            $table->string('field8')->nullable($value =null);
            $table->string('field9')->nullable($value =null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_datas');
    }
}
