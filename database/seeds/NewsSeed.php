<?php

use Illuminate\Database\Seeder;
use App\News;

class NewsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i<20; $i++)
        {
            News::create([
                'title' => str_random(10),
                'short_description' => str_random('80'),
                'description' => str_random('300'),
            ]);
        }
    }
}
