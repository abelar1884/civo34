<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeed extends Seeder
{

    public function run()
    {
        /*
        User::create([
            'name' => 'admin',
            'email' => 'ham@gmail.com',
            'password' => Hash::make('123412'),
            'role' => 'admin',
        ]);*/
        for ($i=0; $i<20; $i++)
        {
            User::create([
                'name' => str_random(10),
                'email' => str_random('10').'@gmail.com',
                'password' =>  Hash::make(str_random('13')),
                'role' => 3,
                'token'  => str_random(20)
            ]);
        }
    }
}
