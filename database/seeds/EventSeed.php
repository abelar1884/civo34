<?php

use Illuminate\Database\Seeder;
use App\Event;

class EventSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = [
            0 => 'Вебинар',
            1 => 'Круглый стол',
            2 => 'Лкция'
        ];

        for ($i=0; $i<20; $i++)
        {
            Event::create([
                'title' => str_random(15),
                'short_description' => str_random('40'),
                'description' => str_random('300'),
                'date' => date('Y-m-d H:i:s',  rand(1533000000, 1533907474)),
                'type' => rand(1,9),
            ]);
        }
    }
}
