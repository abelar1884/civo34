<?php

use Illuminate\Database\Seeder;
use App\EventType;

class TypeEventSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = [
            'Бизнес-миссии',
            'Вебинары',
            'Выставки',
            'Конференции',
            'Мастер-классы',
            'Обучающие программы',
            'Семинары',
            'Форумы',
            'Круглые столы'
        ];

        foreach ($type as $item){
            EventType::create([
                'title' => $item

            ]);
        }
    }
}
