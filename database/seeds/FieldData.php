<?php

use Illuminate\Database\Seeder;
use App\RoleField;


class FieldData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1; $i<10; $i++){
            RoleField::create([
                'field' => 'field'.$i,
                'provider' => (bool)rand(0,1),
                'recipient' => (bool)rand(0,1),
            ]);
        }
    }
}
