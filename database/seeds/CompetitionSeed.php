<?php

use Illuminate\Database\Seeder;

use App\Competition;

class CompetitionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i<20; $i++)
        {
            Competition::create([
                'title' => str_random(10),
                'date_start' => date('Y-m-d H:i:s',  rand(1533000000, 1533907474)),
                'date_end' => date('Y-m-d H:i:s', rand(1533000000, 1533907474)),
                'description' => str_random('100'),
            ]);
        }
    }
}
