<?php

use Illuminate\Database\Seeder;

use App\Role;

class UserRoleSeed extends Seeder
{

    public function run()
    {
        $slug = [
            'admin',
            'moderator',
            'user'
        ];

        $title = [
            'Администратор',
            'Менеджер',
            'Пользователь'
        ];


        foreach ($slug as $key => $name)
        {
            $role = Role::create([
                'title'=>$title[$key],
                'slug'=>$name,
            ]);
        }
    }
}
