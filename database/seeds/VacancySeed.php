<?php

use Illuminate\Database\Seeder;
use App\Vacancy;

class VacancySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i<20; $i++)
        {
            Vacancy::create([
                'title' => str_random(10),
                'salary' => rand(10000, 100000),
                'short_description' =>  str_random(80),
                'description' => str_random(250),
            ]);
        }
    }
}
