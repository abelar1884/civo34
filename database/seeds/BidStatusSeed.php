<?php

use Illuminate\Database\Seeder;
use App\BidStatus;

class BidStatusSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $statuses = [
           101 => 'Заявка отправлена',
           102 => 'Заявка обрабатывается',
           109 => 'Заявка отклонена',
           111 => 'Заявка одобрена'
       ];

       foreach ($statuses as $key => $status){
           BidStatus::create([
               'code' => $key,
               'name' => $status
           ]);
       }
    }
}
