<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Collaborator;
use App\Modules\UpLoadImage;
use App\Modules\Flash;
use App\Image;

class CollaboratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_panel.collaborator.index', ['data' => Collaborator::orderBy('id')->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_panel.collaborator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $collab = Collaborator::create($request->all());

        $image = new UpLoadImage($request, $collab, 'collaboration');
        $image->upLoad();

        Flash::create($collab);

        return redirect()->route('collaborator.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Collaborator $collaborator)
    {
        return view('admin_panel.collaborator.edit', ['object' => $collaborator]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collaborator $collaborator)
    {
        if ($request->has('oldImage')) {
            Image::destroy($request->input('oldImage'));
        }

        $collaborator->fill($request->all())->update();

        $image = new UpLoadImage($request, $collaborator, 'collaborator');
        $image->upLoad();

        Flash::edit($collaborator);

        return redirect()->route('collaborator.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collaborator $collaborator)
    {
        Flash::delete($collaborator->delete());
        return redirect()->route('competition.index');
    }
}
