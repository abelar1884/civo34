<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Competition;
use App\Modules\UpLoadDocument;
use App\Modules\UpLoadImage;
use App\Modules\Flash;
use App\Document;
use App\Image;

class CompetitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_panel.competitions.index', ['data' => Competition::orderBy('id')->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_panel.competitions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comp = Competition::create($request->all());

        $doc = new UpLoadDocument($request, $comp, 'competition');
        $doc->upLoad();

        $image = new UpLoadImage($request, $comp, 'competition');
        $image->upLoad();
        Flash::create($comp);

        return redirect()->route('competition.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Competition $competition)
    {
        return view('admin_panel.competitions.edit', ['object' => $competition]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Competition $competition)
    {
        if ($request->has('oldDocument')) {
            Document::destroy($request->input('oldDocument'));
        }

        if ($request->has('oldImage')) {
            Image::destroy($request->input('oldImage'));
        }

        $competition->fill($request->all())->update();

        $doc = new UpLoadDocument($request, $competition, 'competition');
        $doc->upLoad();

        $image = new UpLoadImage($request, $competition, 'competition');
        $image->upLoad();

        Flash::edit($competition);

        return redirect()->route('competition.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Competition $competition)
    {
        Flash::delete($competition->delete());
        return redirect()->route('competition.index');
    }
}