<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PersonalArea extends Controller
{

    public function index()
    {
        if (Auth::user()->token == 'active') {
            return view('personal_area.index');
        }
        else {
            return view('personal_area.active');
        }
    }

}
