<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ActivateController extends Controller
{

    public function activate()
    {
        return view('personal_area.verification');
    }

    public static function verificationUser(User $user=null, $token)
    {
        if($user){
            if($user->token == $token){
                $user->token = 'active';
                $user->save();
                return 'Учетная запись успешно активирована. Вы можете войти в свой <a href="'.route('personal-index').'"">личный кабинет</a>';
            }
            else {
                return 'Данная учетная запись уже активированна';
            }
        }
        else {
            return 'Данный пользователь не зарегестрирован';
        }
    }
}
