<?php

namespace App\Http\Controllers;

use App\Story;
use Illuminate\Http\Request;
use App\Image;
use App\Document;
use App\Modules\UpLoadDocument;
use App\Modules\UpLoadImage;
use App\Modules\Flash;

class StoryController extends Controller
{


    public function index()
    {
        return view('admin_panel.story.index', ['data' => Story::orderBy('id')->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_panel.story.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $story = Story::create($request->all());

        $doc = new UpLoadDocument($request, $story, 'story');
        $doc->upLoad();

        $image = new UpLoadImage($request, $story, 'story');
        $image->upLoad();

        Flash::create($story);

        return redirect()->route('story.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Story $story)
    {
        return view('admin_panel.story.edit', ['object' => $story]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Story $story)
    {
        if ($request->has('oldDocument')) {
            Document::destroy($request->input('oldDocument'));
        }

        if ($request->has('oldImage')) {
            Image::destroy($request->input('oldImage'));
        }

        $story->fill($request->all())->update();

        $doc = new UpLoadDocument($request, $story, 'story');
        $doc->upLoad();

        $image = new UpLoadImage($request, $story, 'story');
        $image->upLoad();

        Flash::edit($story);

        return redirect()->route('story.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Story $story)
    {
        Flash::delete($story->delete());
        return redirect()->route('story.index');
    }
}
