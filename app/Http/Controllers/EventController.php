<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\UpLoadImage;
use App\Modules\UpLoadDocument;
use App\Modules\Flash;
use App\Event;
use App\Document;
use App\Image;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_panel.event.index', ['data' =>  Event::orderBy('id')->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_panel.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = Event::create($request->all());

        $doc = new UpLoadDocument($request, $event, 'event');
        $doc->upLoad();

        $image = new UpLoadImage($request, $event, 'event');
        $image->upLoad();
        Flash::create($event);

        return redirect()->route('event.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return 'hgfd';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('admin_panel.event.edit', ['object' => $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {

        if ($request->has('oldDocument')) {
            Document::destroy($request->input('oldDocument'));
        }

        if ($request->has('oldImage')) {
            Image::destroy($request->input('oldImage'));
        }

        $event->fill($request->all())->update();

        $doc = new UpLoadDocument($request, $event, 'event');
        $doc->upLoad();

        $image = new UpLoadImage($request, $event, 'event');
        $image->upLoad();

        Flash::edit($event);

        return redirect()->route('event.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        Flash::delete($event->delete());
        return redirect()->route('event.index');
    }
}
