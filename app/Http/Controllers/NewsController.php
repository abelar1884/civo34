<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\UpLoadImage;
use App\Modules\UpLoadDocument;
use App\Modules\Flash;
use App\News;
use App\Document;
use App\Image;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_panel.news.index', ['data' =>  News::orderBy('id')->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_panel.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = News::create($request->all());

        $doc = new UpLoadDocument($request, $news, 'news');
        $doc->upLoad();

        $image = new UpLoadImage($request, $news, 'news');
        $image->upLoad();

        Flash::create($news);

        return redirect()->route('news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        return view('admin_panel.news.edit', ['object' => $news]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        if ($request->has('oldDocument')) {
            Document::destroy($request->input('oldDocument'));
        }

        if ($request->has('oldImage')) {
            Image::destroy($request->input('oldImage'));
        }

        $news->fill($request->all())->update();

        $doc = new UpLoadDocument($request, $news, 'news');
        $doc->upLoad();

        $image = new UpLoadImage($request, $news, 'news');
        $image->upLoad();

        Flash::edit($news);

        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        Flash::delete($news->delete());
        return redirect()->route('news.index');
    }
}
