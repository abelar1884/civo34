<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use App\Image;
use App\Modules\UpLoadImage;
use App\Modules\Flash;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_panel.gallery.index', ['data' => Gallery::orderBy('id')->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_panel.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('images')){
            $request['count'] = count($request['images']);
        }
        $img = Gallery::create($request->all());

        $image = new UpLoadImage($request, $img, 'gallery');
        $image->upLoad();

        Flash::create($img);

        return redirect()->route('gallery.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        return view('admin_panel.gallery.edit', ['object' => $gallery]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {

        if ($request->has('oldImage')) {
            Image::destroy($request->input('oldImage'));
            $request['count'] = count($gallery->image);
        }

        if($request->hasFile('images')){
            $request['count'] = count($gallery->image)+count($request['images']);
        }

        $gallery->fill($request->all())->update();

        $image = new UpLoadImage($request, $gallery, 'gallery');
        $image->upLoad();

        Flash::edit($gallery);

        return redirect()->route('gallery.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
