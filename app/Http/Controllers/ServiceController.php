<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Modules\UpLoadDocument;
use App\Modules\UpLoadImage;
use App\Modules\Flash;
use App\Document;
use App\Image;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_panel.service.index', ['data' => Service::orderBy('id')->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_panel.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = Service::create($request->all());

        $doc = new UpLoadDocument($request, $service, 'service');
        $doc->upLoad();

        $image = new UpLoadImage($request, $service, 'service');
        $image->upLoad();
        Flash::create($service);

        return redirect()->route('service.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin_panel.service.edit', ['object' => $service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        if ($request->has('oldDocument')) {
            Document::destroy($request->input('oldDocument'));
        }

        if ($request->has('oldImage')) {
            Image::destroy($request->input('oldImage'));
        }

        $service->fill($request->all())->update();

        $doc = new UpLoadDocument($request, $service, 'service');
        $doc->upLoad();

        $image = new UpLoadImage($request, $service, 'servise');
        $image->upLoad();

        return redirect()->route('service.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        Flash::delete($service->delete());
        return redirect()->route('service.index');
    }
}
