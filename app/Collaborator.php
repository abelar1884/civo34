<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collaborator extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'position'
    ];

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
}
