<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    protected $fillable = [
        'title',
        'short_description',
        'description',
    ];

    public function document()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    public function image()
    {
        return $this->morphMany('App\Image', 'imageable');
    }
}
