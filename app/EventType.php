<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    protected $fillable = [
        'title',
        'event_id'
    ];

    public function events()
    {
        return $this->hasMany('App\Event', 'type');
    }
}
