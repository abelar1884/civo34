<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidStatus extends Model
{
    protected $fillable = [
        'code',
        'name',
    ];

    public function bids()
    {
        return $this->hasMany('App\Bid','status');
    }
}
