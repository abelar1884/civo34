<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'title',
        'short_description',
        'description',
        'type',
        'date'
    ];

    public function image()
    {
        return $this->morphMany('App\Image','imageable');
    }

    public function document()
    {
        return $this->morphMany('App\Document','documentable');
    }

    public function types()
    {
        return $this->belongsTo('App\EventType','type');
    }
}
