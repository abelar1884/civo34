<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name_role',
    ];

    public function UserRole()
    {
        return $this->hasMany('App\User', 'role');
    }
}
