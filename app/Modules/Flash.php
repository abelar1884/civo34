<?php
/**
 * Created by PhpStorm.
 * User: Эдуард
 * Date: 10.08.2018
 * Time: 13:15
 */

namespace App\Modules;

class Flash
{
    public static function create($var)
    {
        if ($var) {
            session()->flash('mess','Данные успешно добавлены.');
        } else {
            session()->flash('mess','Данные не были добавлены.');
        }
    }
    public static function edit($var)
    {
        if ($var) {
            session()->flash('mess','Данные успешно изменены.');
        } else {
            session()->flash('mess','Данные не были изменены.');
        }
    }

    public static function delete($var)
    {
        if ($var) {
            session()->flash('mess','Данные успешно удалены.');
        } else {
            session()->flash('mess', 'Данные не были удалены.');
        }
    }
}