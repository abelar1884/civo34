<?php
/**
 * Created by PhpStorm.
 * User: Эдуард
 * Date: 20.08.2018
 * Time: 11:30
 */

namespace App\Modules;


class UserStatus
{
    public static function status($provider, $recipient)
    {
        if($provider and $recipient){
            return 'Поставщик и потребитель';
        }
        else{
            if ($provider or $recipient){
                return $provider? 'Поставщик': 'Получатель';
            }
            else {
                return 'Данные на указаны';
            }
        }
    }
}