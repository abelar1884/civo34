<?php
/**
 * Created by PhpStorm.
 * User: Эдуард
 * Date: 10.08.2018
 * Time: 13:30
 */

namespace App\Modules;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Image;

class UpLoadImage
{
    public $request;
    public $model;
    public $path;

    public function __construct(Request $request, Model $model, $path)
    {
        $this->request = $request;
        $this->model = $model;
        $this->path = $path;

    }

    public function upLoad()
    {
        if($this->request->hasFile('images')){
            $arrDocuments = [];
            foreach ($this->request->file('images') as $document){
                array_push($arrDocuments, [
                    'title' => $document->getClientOriginalName(),
                    'file' => Storage::put('/images/'.$this->path.'/'.$this->model->id, $document),
                    'imageable_id' => $this->model->id,
                    'imageable_type' => get_class($this->model)
                ]);
            }
            Image::insert($arrDocuments);
        }

    }
}