<?php
/**
 * Created by PhpStorm.
 * User: Эдуард
 * Date: 07.08.2018
 * Time: 13:52
 */

namespace App\Modules;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Document;

class UpLoadDocument
{
    public $request;
    public $model;
    public $path;

    public function __construct(Request $request, Model $model, $path)
    {
        $this->request = $request;
        $this->model = $model;
        $this->path = $path;

    }

    public function upLoad()
    {
        if($this->request->hasFile('documents')){
            $arrDocuments = [];
            foreach ($this->request->file('documents') as $document){
                array_push($arrDocuments, [
                    'title' => $document->getClientOriginalName(),
                    'file' => Storage::put('/documents/'.$this->path.'/'.$this->model->id, $document),
                    'documentable_id' => $this->model->id,
                    'documentable_type' => get_class($this->model)
                ]);
            }
            Document::insert($arrDocuments);
        }

    }
}