<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleField extends Model
{
    protected $fillable = [
        'field',
        'provider',
        'recipient'
    ];

}
