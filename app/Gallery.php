<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = [
        'title',
        'count',
        'description'
    ];

    public function image()
    {
        return $this->morphMany('App\Image','imageable');
    }
}
