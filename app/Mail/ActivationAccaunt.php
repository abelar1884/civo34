<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivationAccaunt extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }


    public function build()
    {
        $activationLink = route('activation', [
            'id' => $this->user->id,
            'token' => $this->user->token,
        ]);

        return $this->subject('Активация аккаунта')
            ->view('mails.activated')->with([
                'link' => $activationLink
            ]);

    }
}
