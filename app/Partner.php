<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $fillable = [
        'title',
        'link',
        'description',
    ];

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
}
