<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'title',
        'file',
        'documentable_id',
        'documentable_type',
    ];

    public function documetable()
    {
        return $this->morphTo();
    }
}
