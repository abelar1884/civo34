<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class News extends Model
{
    protected $fillable = [
        'title',
        'short_description',
        'description'
    ];

    public function image()
    {
        return $this->morphMany('App\Image','imageable');
    }

    public function document()
    {
        return $this->morphMany('App\Document','documentable');
    }
}
