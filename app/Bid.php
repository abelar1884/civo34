<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $fillable = [
        'user_create',
        'status',
        'dscription',
        'bidable_id',
        'bidable_type'
    ];

    public function bidable()
    {
        return $this->morphTo();
    }

    public function document()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    public function statuses()
    {
        return $this->belongsTo('App\BidStatus','status');
    }

}
