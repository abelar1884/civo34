<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    protected $fillable = [
        'title',
        'date_start',
        'date_end',
        'description',
    ];

    public function document()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    public function image()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function bid()
    {
        return $this->morphMany('App\Bid', 'bidable');
    }
}
