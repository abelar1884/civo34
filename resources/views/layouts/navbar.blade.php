
<nav class="navbar navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="{{ route('index') }}">ЦИВО</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('index') }}">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('competitions') }}">Кокурсы</a>
            </li>
            <li class="nav-item" style="position: absolute; right: 0">
                @if(Auth::check())
                    <a class="nav-link" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i> Выйти</a>
                @else
                    <a class="nav-link" href="{{ route('home') }}"><i class="fas fa-sign-in-alt"></i> Личный кабинет</a>
                @endif
            </li>
            <li class="nav-item">
                @if(Auth::check())

                @else
                    <a class="nav-link" href="{{ route('register') }}">Регистрация</a>
                @endif
            </li>
        </ul>
    </div>
</nav>
