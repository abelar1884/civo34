@extends('admin_panel.layout')

@section('title', 'Галерея')

@section('create')
    <a class="btn btn-success" href="{{ route('gallery.create')}}">Добавить </a>
@endsection

@section('content')
    <table class="table table-dark" style="margin-top: 20px;">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название альбома</th>
            <th scope="col">Кол-во фотографий</th>
            <th scope="col">Дата добавления</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td> <a href="{{ route('gallery.edit', ['id'=> $item->id]) }}"> {{$item->title}} </a> </td>
                <td>{{ $item->count }}</td>
                <td>{{ $item->created_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $data->render() }}
@endsection