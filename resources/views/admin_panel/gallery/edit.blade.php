@extends('admin_panel.layout')

@section('title', 'Редактировать конкурс')

@section('content')
    <h3>{{ $object->title }}</h3>
    <form method="post" action="{{ route('vacancy.destroy', ['id' => $object->id]) }}">
        <input type="hidden" name="_method" value="delete" >
        @csrf
        <button type="submit" class="btn btn-danger" id="deleted">Удалить</button>
    </form>

    <div class="create-update">
        <p><b>Дата добавления:</b><br> {{ $object->created_at }}</p>
        <p><b>Дата изменения:</b><br> {{ $object->updated_at }}</p>
    </div>

    {!! Form::open(['route' => ['gallery.update', 'competition' => $object->id], 'method' => 'PUT', 'files' => true, 'id' => 'sb-form']) !!}

        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $object->title }}">
        </div>

        <div class="form-group">
            {{ Form::label('oldImage', 'Изображения', ['class' => 'h5']) }}
            <span>Поставте галочку если хотите удалить файл</span>
            <br>

            @foreach($object->image as $image)
                <div class="images">
                    <img src="/storage/{{$image->file}}" width="100" height="100">
                    <div class="delite-images">
                        <input type="text" value="{{ $image->title }}" style="width: 100%" class="form-control">
                        <input type="checkbox" value="{{ $image->id }}" name="oldImage[]"> Удалить
                    </div>
                </div>
            @endforeach
        </div>

        <div class="form-group" style="width: 100%;">
            {{ Form::label('images', 'Прирепить изображение', ['class' => 'h5']) }}<br>
            <input id="image-id" type="file" name="images[]" multiple class="file" data-preview-file-type="text">
        </div>

        <div class="form-group">
            <label for="summernote">Описание </label><br>
            <textarea id="summernote" name="description">{{ $object->description }}</textarea>
        </div>

        <button type="submit" id="sb-update" class="btn btn-success">Обновить</button>
    {!! Form::close() !!}
@endsection

@section('script')
    <script>
        $('#summernote').summernote({
            lang: 'ru-RU',
            placeholder: 'Описание мероприятия',
            tabsize: 2,
            height: 200
        });

        $('#summernote_short').summernote({
            lang: 'ru-RU',
            placeholder: 'Описание мероприятия',
            tabsize: 2,
            height: 200
        });


    </script>
@endsection