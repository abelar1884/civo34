@extends('admin_panel.layout')

@section('title', 'Создать галерею')

@section('content')

    <h3>Создать галерею</h3>
    {!! Form::open(['route' => ['gallery.store'], 'files' => true]) !!}

        <div class="form-group">
            <label for="title">Название</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>

        <div class="form-group">
            <label for="images">Прикрепить фото</label><br>
            <input type="file" name="images[]" multiple>
        </div>



        <div class="form-group">
            <label for="summernote">Описание</label><br>
            <textarea id="summernote" name="description"></textarea>
        </div>

        <button type="submit" id="sb-update" class="btn btn-success">Добавить</button>
    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#summernote').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });

            $('input[type=file]').change(function () {
                fileCount = this.files.length;
                $(this).prev().text(fileCount + 'Selected');
            })
        });
    </script>
@endsection