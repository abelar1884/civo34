@extends('admin_panel.layout')

@section('title', 'Добавить нового партнера')

@section('content')

    <h3>Добавить новый конкурс</h3>
    {!! Form::open(['route' => ['partner.store'], 'files' => true]) !!}

        <div class="form-group">
            <label for="title">Название</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>

        <div class="form-group">
            <label for="title">Ссылка</label>
            <input type="text" class="form-control" id="title" name="link">
        </div>

        <div class="form-group">
            <label for="images">Добавить логоттип</label><br>
            <input type="file" name="images[]">
        </div>

        <div class="form-group">
            <label for="summernote">Описание</label><br>
            <textarea id="summernote" name="description"></textarea>
        </div>

        <button type="submit" id="sb-update" class="btn btn-success">Добавить</button>
    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#summernote').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });
        });
    </script>
@endsection