@extends('admin_panel.layout')

@section('title', 'Партнеры')

@section('create')
    <a class="btn btn-success" href="{{ route('partner.create')}}">Добавить партнера</a>
@endsection

@section('content')
    <table class="table table-dark" style="margin-top: 20px;">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Имя</th>
            <th scope="col">Сылка</th>
            <th scope="col">Лого</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td> <a href="{{ route('partner.edit', ['id'=> $item->id]) }}"> {{$item->title}} </a> </td>
                <td>{{$item->link}}</td>
                <td><img src="/storage/{{$item->image->file}}" style="width: 250px; height: 60px;"></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $data->render() }}
@endsection