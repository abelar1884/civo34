@extends('admin_panel.layout')

@section('title', 'Добавить историю')

@section('content')

    <h3>Добавить услугу</h3>
    {!! Form::open(['route' => ['story.store'], 'files' => true]) !!}

        <div class="form-group">
            <label for="title">Название</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>

        <div class="form-group">
            <label for="summernote">Краткое описание</label><br>
            <textarea id="summernote_short" name="short_description"></textarea>
        </div>

        <div class="form-group">
            <label for="summernote">Описание</label><br>
            <textarea id="summernote" name="description"></textarea>
        </div>

        <div class="form-group">
            <label for="images">Прикрепить фото</label><br>
            <input type="file" name="images[]" multiple>
        </div>

        <div class="form-group">
            <label for="documents">Прикрепить документы</label><br>
            <input type="file" name="documents[]" multiple>
        </div>

        <button type="submit" id="sb-update" class="btn btn-success">Добавить</button>
    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#summernote').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });
            $('#summernote_short').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });
        });
    </script>
@endsection