@extends('admin_panel.layout')

@section('title', 'Истории успеха')

@section('create')
    <a class="btn btn-success" href="{{ route('story.create')}}">Добавить историю</a>
@endsection

@section('content')
    <table class="table table-dark" style="margin-top: 20px;">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Дата добавления</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td> <a href="{{ route('story.edit', ['id'=> $item->id]) }}"> {{$item->title}} </a> </td>
                <td>{{$item->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $data->render() }}
@endsection