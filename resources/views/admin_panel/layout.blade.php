<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin | @yield('title')</title>


    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sb-admin.css')}}" rel="stylesheet">
    <link href="{{asset('/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('/css/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('/css/summernote-bs4.css')}}" rel="stylesheet">
    <link href="{{asset('/css/icon-file.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

</head>

<body id="page-top">

@if($flash = session('mess'))
<div id="messege" class="alert alert-success" role="alert" style="position: absolute; z-index: 99999;">
   {{ $flash }}
</div>
@endif

<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="">Панель администратора</a>
    <a href="{{ route('logout') }}" id="logout" class="btn btn-danger">Выход</a>
</nav>

<div id="wrapper">

    <ul class="sidebar navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('sb-index') }}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Главная</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('competition.index') }}">
                <i class="far fa-list-alt"></i>
                <span>Кокурсы</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('news.index') }}">
                <i class="far fa-newspaper"></i>
                <span>Новости</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('event.index') }}">
                <i class="far fa-calendar-alt"></i>
                <span>Мероприятия</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('service.index') }}">
                <i class="fas fa-tasks"></i>
                <span>Услуги</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('collaborator.index') }}">
                <i class="fas fa-user-tie"></i>
                <span>Сотрудники</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('vacancy.index') }}">
                <i class="fas fa-chart-line"></i>
                <span>Вакансии</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('gallery.index') }}">
                <i class="far fa-images"></i>
                <span>Галерея</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('user.index') }}">
                <i class="fas fa-users"></i>
                <span>Пользователи</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('story.index') }}">
                <i class="far fa-thumbs-up"></i>
                <span>Истории успеха</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('faq.index') }}">
                <i class="far fa-question-circle"></i>
                <span>Вопрос-ответ</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('partner.index') }}">
                <i class="far fa-handshake"></i>
                <span>Партнеры</span>
            </a>
        </li>
    </ul>

    <div class="sb-content">
        @yield('create')
        @yield('content')
    </div>

</div>

    <script src="{{ asset('/js/jquery-slim.min.js') }}"></script>
    <script src="{{ asset('/js/summernote.js') }}" ></script>
    <script src="{{ asset('/js/jquery-3.3.1.min.js') }}" ></script>
    <script src="{{ asset('/js/summernote-bs4.js') }}" ></script>
    <script src="{{ asset('/js/popper.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/summernote-ru-RU.js') }}"></script>

    <script>
        $(window).ready(function () {
            $('#messege').delay(3000).fadeOut(350);
        });
    </script>
    @yield('script')
</body>
</html>
