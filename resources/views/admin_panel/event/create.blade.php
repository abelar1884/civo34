@extends('admin_panel.layout')

@section('title', 'Создать новое мероприятие')

@section('content')

    <h3>Добавить новый конкурс</h3>
    {!! Form::open(['route' => ['event.store'], 'files' => true]) !!}

    <div class="form-group">
        <label for="title">Название</label>
        <input type="text" class="form-control" id="title" name="title">
    </div>

    <div class="form-group">
        <label for="type">Тип мероприятия</label>
        <select class="form-control" name="type" id="type">
            @foreach(\App\EventType::all() as $item)
                <option value="{{ $item->id }}">{{ $item->title }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group" style="width: 20%;float: left; display: inline-block; margin-right: 20px;">
        <label for="date_start">Дата проведения</label>
        <input type="datetime-local" class="form-control" id="dat" name="date">
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>

    <div class="form-group">
        <label for="summernote">Краткое описание</label><br>
        <textarea id="summernote_short" name="short_description"></textarea>
    </div>

    <div class="form-group">
        <label for="summernote">Описание</label><br>
        <textarea id="summernote" name="description"></textarea>
    </div>

    <div class="form-group">
        <label for="images">Прикрепить фото</label><br>
        <input type="file" name="images[]" multiple>
    </div>

    <div class="form-group">
        <label for="documents">Прикрепить документы</label><br>
        <input type="file" name="documents[]" multiple>
    </div>

    <button type="submit" id="sb-update" class="btn btn-success">Добавить</button>
    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#summernote').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });

            $('#summernote_short').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });
        });
    </script>

@endsection