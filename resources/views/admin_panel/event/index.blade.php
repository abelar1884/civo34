@extends('admin_panel.layout')

@section('title', 'Мероприятия')

@section('create')
    <a class="btn btn-success" href="{{ route('event.create')}}">Добавить мероприятие</a>
@endsection

@section('content')
    <table class="table table-dark" style="margin-top: 20px;">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Тип</th>
            <th scope="col">Дата проведения</th>

        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td> <a href="{{ route('event.edit', ['id'=> $item->id]) }}"> {{$item->title}} </a> </td>
                <td>{{$item->types->title}}</td>
                <td>{{$item->date}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $data->render() }}
@endsection