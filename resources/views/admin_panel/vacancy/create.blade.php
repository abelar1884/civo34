@extends('admin_panel.layout')

@section('title', 'Добавить вакансию')

@section('content')

    <h3>Добавить новую вакансию</h3>
    {!! Form::open(['route' => ['vacancy.store'], 'files' => true]) !!}

        <div class="form-group">
            <label for="title">Название</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>

        <div class="form-group">
            <label for="title">З/П</label>
            <input type="number" class="form-control" id="salary" name="salary">
        </div>

        <div class="form-group">
            <label for="summernote">Описание конкурса</label><br>
            <textarea id="summernote_short" name="short_description"></textarea>
        </div>

        <div class="form-group">
            <label for="summernote">Описание конкурса</label><br>
            <textarea id="summernote" name="description"></textarea>
        </div>

        <button type="submit" id="sb-update" class="btn btn-success">Добавить</button>
    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#summernote').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });

        });

        $(window).ready(function () {
            $('#summernote_short').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });

        });
    </script>
@endsection