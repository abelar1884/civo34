@extends('admin_panel.layout')

@section('title', 'Вакинсии')

@section('create')
    <a class="btn btn-success" href="{{ route('vacancy.create')}}">Добавить </a>
@endsection

@section('content')
    <table class="table table-dark" style="margin-top: 20px;">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">ЗП</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td> <a href="{{ route('vacancy.edit', ['id'=> $item->id]) }}"> {{$item->title}} </a> </td>
                <td>{{$item->salary}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $data->render() }}
@endsection