@extends('admin_panel.layout')

@section('title', 'Конкурсы')

@section('create')
    <a class="btn btn-success" href="{{ route('competition.create')}}">Добавить конкурс</a>
@endsection

@section('content')
    <table class="table table-dark" style="margin-top: 20px;">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Дата начала</th>
            <th scope="col">Дата окончания</th>
            <th scope="col">Статус</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td> <a href="{{ route('competition.edit', ['id'=> $item->id]) }}"> {{$item->title}} </a> </td>
                <td>{{$item->date_start}}</td>
                <td>{{$item->date_end}}</td>
                @if( date('U') < date('U', strtotime($item->date_end)) )
                <td>Идет</td>
                @else
                <td>Окончен</td>
                @endif

            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $data->render() }}
@endsection