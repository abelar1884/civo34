@extends('admin_panel.layout')

@section('title', 'Редактировать конкурс')

@section('content')
    <h3>{{ $object->title }}</h3>
    <form method="post" action="{{ route('competition.destroy', ['id' => $object->id]) }}">
        <input type="hidden" name="_method" value="delete" >
        @csrf
        <button type="submit" class="btn btn-danger" id="deleted">Удалить</button>
    </form>

    <div class="create-update">
        <p><b>Дата добавления:</b><br> {{ $object->created_at }}</p>
        <p><b>Дата изменения:</b><br> {{ $object->updated_at }}</p>
    </div>

    {!! Form::open(['route' => ['competition.update', 'competition' => $object->id], 'method' => 'PUT', 'files' => true, 'id' => 'sb-form']) !!}

        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $object->title }}">
        </div>

        <div class="form-group">
            {{ Form::label('oldImage', 'Изображения', ['class' => 'h5']) }}
            <span>Поставте галочку если хотите удалить файл</span>
            <br>

            @foreach($object->image as $image)
                <div class="images">
                    <img src="/storage/{{$image->file}}" width="100" height="100">
                    <div class="delite-images">
                        <input type="checkbox" value="{{ $image->id }}" name="oldImage[]"> Удалить
                    </div>
                </div>
            @endforeach
        </div>

        <div class="form-group" style="width: 100%;">
            {{ Form::label('images', 'Прирепить изображение', ['class' => 'h5']) }}<br>
            <input id="image-id" type="file" name="images[]" multiple class="file" data-preview-file-type="text">
        </div>

        <label>Время проведения конкурса</label><br>
        <div class="form-group" style="width: 25%;float: left; display: inline-block; margin-right: 20px;">
            <label for="date_start">Дата начала</label>
            <input type="datetime-local" class="form-control" id="date_start" name="date_start" value="{{ date('Y-m-d',strtotime($object->date_start)) }}T{{ date('H:i',strtotime($object->date_start)) }}">
        </div>
        <div class="form-group" style="width: 25%; display: inline-block">
            <label for="date_end">Дата окончания</label>
            <input type="datetime-local" class="form-control" id="date_end" name="date_end" value="{{ date('Y-m-d',strtotime($object->date_end)) }}T{{ date('H:i',strtotime($object->date_end)) }}">
        </div>

        <div class="form-group">
            <label for="summernote">Описание конкурса</label><br>
            <textarea id="summernote" name="description">{{ $object->description }}</textarea>
        </div>

        <div class="form-group">
            {{ Form::label('oldDocument', 'Прикрепленные документы', ['class' => 'h5']) }}
            <span>Поставте галочку если хотите удалить файл</span>
            <br>

            @foreach( $object->document as $document)
                <div class="documents">
                    <input type="checkbox" value="{{ $document->id }}" name="oldDocument[]">
                    <a href="/storage/{{ $document->file }}" ><span>{{ $document->title }}</span></a>
                </div>
            @endforeach
        </div>

        <div class="form-group">
            {{ Form::label('documents', 'Документы', ['class' => 'h5']) }}
            <input id="input-id" type="file" name="documents[]" multiple class="file">
        </div>


        <button type="submit" id="sb-update" class="btn btn-success">Обновить</button>
    {!! Form::close() !!}
@endsection

@section('script')
    <script>
        $('#summernote').summernote({
            lang: 'ru-RU',
            placeholder: 'Описание мероприятия',
            tabsize: 2,
            height: 200
        });

    </script>
@endsection