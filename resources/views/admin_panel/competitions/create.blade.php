@extends('admin_panel.layout')

@section('title', 'Создать новый конкурс')

@section('content')

    <h3>Добавить новый конкурс</h3>
    {!! Form::open(['route' => ['competition.store'], 'files' => true]) !!}

        <div class="form-group">
            <label for="title">Название</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>

        <label>Время проведения конкурса</label><br>
        <div class="form-group" style="width: 20%;float: left; display: inline-block; margin-right: 20px;">
            <label for="date_start">Дата начала</label>
            <input type="datetime-local" class="form-control" id="date_start" name="date_start">
        </div>
        <div class="form-group" style="width: 20%; display: inline-block">
            <label for="date_end">Дата окончания</label>
            <input type="datetime-local" class="form-control" id="date_end" name="date_end">
        </div>

        <div class="form-group">
            <label for="summernote">Описание конкурса</label><br>
            <textarea id="summernote" name="description"></textarea>
        </div>

        <div class="form-group">
            <label for="images">Прикрепить фото</label><br>
            <input type="file" name="images[]" multiple>
        </div>

        <div class="form-group">
            <label for="documents">Прикрепить документы</label><br>
            <input type="file" name="documents[]" multiple>
        </div>

        <button type="submit" id="sb-update" class="btn btn-success">Добавить</button>
    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#summernote').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });

        });
    </script>
@endsection