@extends('admin_panel.layout')

@section('title', 'Сотрудники')

@section('create')
    <a class="btn btn-success" href="{{ route('collaborator.create')}}">Добавить сотрудника</a>
@endsection

@section('content')
    <table class="table table-dark" style="margin-top: 20px;">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ФИО</th>
            <th scope="col">Должность</th>
            <th scope="col">E-mail</th>
            <th scope="col">Телефон</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td> <a href="{{ route('collaborator.edit', ['id'=> $item->id]) }}"> {{$item->name}} </a> </td>
                <td>{{$item->position}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->phone}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $data->render() }}
@endsection