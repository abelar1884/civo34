@extends('admin_panel.layout')

@section('title', 'Редактировать информацию сотрудника')

@section('content')
    <h3>{{ $object->name }}</h3>
    <form method="post" action="{{ route('collaborator.destroy', ['id' => $object->id]) }}">
        <input type="hidden" name="_method" value="delete" >
        @csrf
        <button type="submit" class="btn btn-danger" id="deleted">Удалить</button>
    </form>

    <div class="create-update">
        <p><b>Дата добавления:</b><br> {{ $object->created_at }}</p>
        <p><b>Дата изменения:</b><br> {{ $object->updated_at }}</p>
    </div>

    {!! Form::open(['route' => ['competition.update', 'competition' => $object->id], 'method' => 'PUT', 'files' => true, 'id' => 'sb-form']) !!}

    <div class="form-group">
        <label for="name">ФИО</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $object->name }}">
    </div>

    <div class="form-group">
        <label for="name">Должность</label>
        <input type="text" class="form-control" id="position" name="position" value="{{ $object->position }}">
    </div>

    <div class="form-group">
        <label for="name">E-Mail</label>
        <input type="email" class="form-control" id="email" name="email" value="{{ $object->email }}">
    </div>

    <div class="form-group">
        <div class="images">
            <img src="/storage/{{$object->image}}" width="100" height="100">
            <div class="delite-images">
                <input type="checkbox" value="{{ $object->image }}" name="oldImage[]"> Удалить
            </div>
        </div>
    </div>

    <div class="form-group" style="width: 100%;">
        {{ Form::label('images', 'Прирепить изображение', ['class' => 'h5']) }}<br>
        <input id="image-id" type="file" name="images[]" multiple class="file" data-preview-file-type="text">
    </div>


    <button type="submit" id="sb-update" class="btn btn-success">Обновить</button>
    {!! Form::close() !!}
@endsection

@section('script')

@endsection