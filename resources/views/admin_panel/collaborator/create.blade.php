@extends('admin_panel.layout')

@section('title', 'Добавить нового сотрудника')

@section('content')

    <h3>Добавить новый конкурс</h3>
    {!! Form::open(['route' => ['collaborator.store'], 'files' => true]) !!}

    <div class="form-group">
        <label for="title">ФИО</label>
        <input type="text" class="form-control" id="name" name="name">
    </div>

    <div class="form-group">
        <label for="title">Должность</label>
        <input type="text" class="form-control" id="position" name="position">
    </div>

    <div class="form-group">
        <label for="title">E-Mail</label>
        <input type="email" class="form-control" id="email" name="email">
    </div>

    <div class="form-group">
        <label for="title">Телефон</label>
        <input type="number" class="form-control" id="phone" name="phone">
    </div>

    <div class="form-group">
        <label for="images">Прикрепить фото</label><br>
        <input type="file" name="images[]">
    </div>

    <button type="submit" id="sb-update" class="btn btn-success">Добавить</button>
    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#summernote').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });

        });
    </script>
@endsection