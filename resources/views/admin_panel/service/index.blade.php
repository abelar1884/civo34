@extends('admin_panel.layout')

@section('title', 'Услуги')

@section('create')
    <a class="btn btn-success" href="{{ route('service.create')}}">Добавить услугу</a>
@endsection

@section('content')
    <table class="table table-dark" style="margin-top: 20px;">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Тип услуги</th>
            <th scope="col">Дата добавления</th>

        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td> <a href="{{ route('news.edit', ['id'=> $item->id]) }}"> {{$item->title}} </a> </td>
                <td>{{$item->short_description}}</td>
                <td>{{$item->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $data->render() }}
@endsection