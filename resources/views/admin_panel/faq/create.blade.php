@extends('admin_panel.layout')

@section('title', 'Добавить новый вопрос-ответ')

@section('content')

    <h3>Добавить новый конкурс</h3>
    {!! Form::open(['route' => ['faq.store'], 'files' => true]) !!}

        <div class="form-group">
            <label for="title">Название</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>


        <div class="form-group">
            <label for="summernote">Вопрос</label><br>
            <textarea id="summernote" name="question"></textarea>
        </div>

        <div class="form-group">
            <label for="summernote">Ответ</label><br>
            <textarea id="summernote_short" name="answer"></textarea>
        </div>



        <button type="submit" id="sb-update" class="btn btn-success">Добавить</button>
    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#summernote').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });
            $('#summernote_short').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });

        });
    </script>
@endsection