@extends('admin_panel.layout')

@section('title', 'Редактировать конкурс')

@section('content')
    <h3>{{ $object->title }}</h3>
    <form method="post" action="{{ route('faq.destroy', ['id' => $object->id]) }}">
        <input type="hidden" name="_method" value="delete" >
        @csrf
        <button type="submit" class="btn btn-danger" id="deleted">Удалить</button>
    </form>

    <div class="create-update">
        <p><b>Дата добавления:</b><br> {{ $object->created_at }}</p>
        <p><b>Дата изменения:</b><br> {{ $object->updated_at }}</p>
    </div>

    {!! Form::open(['route' => ['faq.update', 'competition' => $object->id], 'method' => 'PUT', 'files' => true, 'id' => 'sb-form']) !!}

        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $object->title }}">
        </div>

        <div class="form-group">
            <label for="summernote">Вопрос ответ</label><br>
            <textarea id="summernote" name="question">{{ $object->question }}</textarea>
        </div>

        <div class="form-group">
            <label for="summernote">Ответ</label><br>
            <textarea id="summernote_short" name="answer">{{ $object->answer }}</textarea>
        </div>

        <button type="submit" id="sb-update" class="btn btn-success">Обновить</button>
    {!! Form::close() !!}
@endsection

@section('script')
    <script>
        $('#summernote').summernote({
            lang: 'ru-RU',
            placeholder: 'Описание мероприятия',
            tabsize: 2,
            height: 200
        });

        $('#summernote_short').summernote({
            lang: 'ru-RU',
            placeholder: 'Описание мероприятия',
            tabsize: 2,
            height: 200
        });

    </script>
@endsection