@extends('admin_panel.layout')

@section('title', 'Добавить пользователя')

@section('content')

    <h3>Добавить нового пользователя</h3>

    <!-- Вывод ошибок при валидации !-->

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(['route' => ['user.store'], 'files' => true]) !!}

        <div class="form-group">
            <label for="title">ФИО</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>

        <div class="form-group">
            <label for="title">E-Mail</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>

        <div class="form-group">
            <label for="title">Пароль</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>

        <div class="form-group">
            <label for="title">Повторите пароль</label>
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
        </div>

        <div class="form-group">
            <label for="title">Тип пользователя</label>
            <select id="role" class="form-control" name="role">
                <option value="2">Модератор</option>
                <option value="3">Пользователь</option>
            </select>
        </div>

        <div class="user" style="display: none;">
        @for($i=0; $i<7; $i++)
            <div class="form-group">
                <label for="title">Поле {{ $i }}</label>
                <input type="text" class="form-control" name="field{{$i}}">
            </div>
        @endfor
        </div>
        <button type="submit" id="sb-update" class="btn btn-success">Добавить</button>
    {!! Form::close() !!}


@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#role').change(function () {
                if($('#role').val() == 3){
                    $('.user').css('display', 'block');
                }
                else {
                    $('.user').css('display', 'none');
                }
            });
        });
    </script>
@endsection