@extends('admin_panel.layout')

@section('title', 'Редактировать конкурс')

@section('content')
    <h3>{{ $object->name }}</h3>

    <form method="post" action="{{ route('user.destroy', ['id' => $object->id]) }}">
        <input type="hidden" name="_method" value="delete" >
        @csrf
        <button type="submit" class="btn btn-danger" id="deleted">Удалить</button>
    </form>

    <div class="create-update">
        <p><b>Дата добавления:</b><br> {{ $object->created_at }}</p>
        <p><b>Дата изменения:</b><br> {{ $object->updated_at }}</p>
    </div>

    {!! Form::open(['route' => ['user.update', 'user' => $object->id], 'method' => 'PUT', 'files' => true, 'id' => 'sb-form']) !!}

        <div class="form-group">
            <label for="name">ФИО</label>
            <input type="text" class="form-control" id="title" name="name" value="{{ $object->name }}">
        </div>

        <div class="form-group">
            <label for="name">Роль</label>
            <select name="role" class="form-control" id="role">
                @foreach(\App\Role::all() as $role)
                    @if($role->id == $object->roles->id)
                        <option value="{{$role->id}}" selected>{{ $role->title }}</option>
                    @else
                        <option value="{{$role->id}}">{{ $role->title }}</option>
                    @endif
                @endforeach
            </select>
        </div>

        <div class="user" style="display: {{ $object->roles->id == 3?'block':'none' }};">
            @for($i=0; $i<7; $i++)
                <div class="form-group">
                    <label for="title">Поле {{ $i }}</label>
                    <input type="text" class="form-control" name="field{{$i}}">
                </div>
            @endfor
        </div>



        <button type="submit" id="sb-update" class="btn btn-success">Обновить</button>
    {!! Form::close() !!}
@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#role').change(function () {
                if($('#role').val() == 3){
                    $('.user').css('display', 'block');
                }
                else {
                    $('.user').css('display', 'none');
                }

            });
        });
    </script>
@endsection