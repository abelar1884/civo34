@extends('admin_panel.layout')

@section('title', 'Пользователи')

@section('create')
    <a class="btn btn-success" href="{{ route('user.create')}}">Добавить пользователя</a>
@endsection

@section('content')
    <table class="table table-dark" style="margin-top: 20px;">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ФИО</th>
            <th scope="col">Роль</th>
            <th scope="col">Стаус</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td> <a href="{{ route('user.edit', ['id'=> $item->id]) }}"> {{$item->name}} </a> </td>
                <td>{{$item->roles->title}}</td>
                <td>{{\App\Modules\UserStatus::status($item->provider, $item->recipient)}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $data->render() }}
@endsection