<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sb-admin.css')}}" rel="stylesheet">
    <link href="{{asset('/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('/css/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('/css/summernote-bs4.css')}}" rel="stylesheet">
    <link href="{{asset('/css/icon-file.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <title>Добро пожаловать {{ Auth::user()->name }}</title>

</head>
<body>
    <div class="container">
        @include('layouts.navbar')
        <div class="container personal">
        @include('personal_area.components.navbar')
        @yield('content')
        <div class="container personal">
    </div>
    <script src="{{ asset('/js/jquery-slim.min.js') }}"></script>
    <script src="{{ asset('/js/summernote.js') }}" ></script>
    <script src="{{ asset('/js/jquery-3.3.1.min.js') }}" ></script>
    <script src="{{ asset('/js/summernote-bs4.js') }}" ></script>
    <script src="{{ asset('/js/popper.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/summernote-ru-RU.js') }}"></script>

    @yield('script')
    <script>
        $(window).ready(function () {
            $('#messege').delay(3000).fadeOut(350);
        });
    </script>
</body>
</html>
