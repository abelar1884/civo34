@extends('welcome')

@section('title', 'Подтверждние')

@section('content')
    <?php
        use App\Http\Controllers\ActivateController;

        echo ActivateController::verificationUser(App\User::find($_GET['id']), $_GET['token']);

    ?>

@endsection