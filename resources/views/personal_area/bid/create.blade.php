@extends('personal_area.layout')

@section('title', 'Отправить заявку на конкурс')

@section('content')
    <div class="content">
    <h3>Отправить заявку на конкурс</h3>
    {!! Form::open(['route' => ['bid.store'], 'files' => true]) !!}

        <div class="form-group">
            <label for="summernote">Выберите конкурс</label><br>
            <select class="form-control" name="bidable_id">
                @foreach(\App\Competition::all() as $comp)
                    <option value="{{ $comp->id }}">{{ $comp->title }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="documents">Прикрепить документы</label><br>
            <input type="file" name="documents[]" multiple>
        </div>

        <div class="form-group">
            <input type="number" name="user_create" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}" hidden>
            <input type="text" name="bidable_type" value="App\Competition" hidden>
            <input type="number" name="status" value="2" hidden>
        </div>


        <div class="form-group">
            <label for="summernote">Дополнительная информация</label><br>
            <textarea id="summernote" name="description"></textarea>
        </div>

        <button type="submit" id="sb-update" class="btn btn-success">Добавить</button>
    {!! Form::close() !!}
    </div>
@endsection

@section('script')
    <script>
        $(window).ready(function () {
            $('#summernote').summernote({
                lang: 'ru-RU',
                placeholder: 'Описание мероприятия',
                tabsize: 2,
                height: 200
            });
        });
    </script>
@endsection