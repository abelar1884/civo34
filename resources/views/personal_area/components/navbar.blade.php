<div class="menu">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('bid.create') }}">Подать заявку на конкурс</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Вебинары</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Стать оставщиком</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Получить услугу</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Задать вопрос</a>
        </li>
    </ul>
</div>